ToDo Web App made with VueJS for frontend, Gin-Gonic Golang for backend and PostgreSQL for database.

--To run this app you need PostgreSQL user "postgres", with database named "db", password "777111777", and table named "tasks" with 1 column "data", which has type character varying.

1. Run "omg.go" inside backend folder to host backend API.
2. Run "index.html" inside frontend folder on localhost with whatever port but "8080", because it's used by backend.
3. Open your localhost in your browser.
