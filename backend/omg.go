package main

import (
	"database/sql"
	"net/http"
	"strconv"

	"github.com/Bisstocuz/gin"
	_ "github.com/lib/pq"
)

type todo struct {
	Data string `json:"data"`
}

var todoList = []todo{}

func main() {
	router := gin.Default()
	router.Use(CORSMiddleware())
	router.GET("/list", getData)
	router.POST("/list", postData)
	router.PATCH("/list/:index", editData)
	router.DELETE("/list/:index", deleteData)

	router.Run("localhost:8080")
}

func getData(c *gin.Context) {
	connStr := "user=postgres password=777111777 dbname=db sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	var data todo

	rows, err := db.Query("SELECT data FROM tasks")
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	todoList = nil
	for rows.Next() {
		err := rows.Scan(&data.Data)
		if err != nil {
			panic(err)
		}
		todoList = append(todoList, data)
	}
	err = rows.Err()
	if err != nil {
		panic(err)
	}
	c.IndentedJSON(http.StatusOK, todoList)
}

func postData(c *gin.Context) {
	var newData todo
	if err := c.BindJSON(&newData); err != nil {
		return
	}

	connStr := "user=postgres password=777111777 dbname=db sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	todoList = append(todoList, newData)

	sqlStatement := `
		INSERT INTO tasks (data)
		VALUES ($1)`
	_, err = db.Exec(sqlStatement, newData.Data)
	if err != nil {
		panic(err)
	}

	c.IndentedJSON(http.StatusCreated, newData)
}

func editData(c *gin.Context) {
	connStr := "user=postgres password=777111777 dbname=db sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	indexStr := c.Param("index")
	var newData todo
	if err := c.BindJSON(&newData); err != nil {
		return
	}
	count := 0
	for _, a := range todoList {
		if index, _ := strconv.Atoi(indexStr); index == count {
			sqlStatementUpdt := `
    			UPDATE tasks
    			SET data = $2 WHERE data = $1;`
			_, err := db.Exec(sqlStatementUpdt, todoList[index].Data, newData.Data)
			if err != nil {
				panic(err)
			}
			todoList[index].Data = newData.Data
			c.IndentedJSON(http.StatusOK, a)
			return
		}
		count++
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "index not found"})
}

func deleteData(c *gin.Context) {
	connStr := "user=postgres password=777111777 dbname=db sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	indexStr := c.Param("index")
	count := 0

	for _, a := range todoList {
		if index, _ := strconv.Atoi(indexStr); index == count {
			sqlStatementDel := `
    			DELETE FROM tasks
    			WHERE data = $1;`
			_, err := db.Exec(sqlStatementDel, todoList[index].Data)
			if err != nil {
				panic(err)
			}
			todoList = append(todoList[:index], todoList[index+1:]...)
			c.IndentedJSON(http.StatusOK, a)
			return
		}
		count++
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "index not found"})
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE, PATCH")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
