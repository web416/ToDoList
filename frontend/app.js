var app = new Vue({
    el: '#vue',
    data: {
      curAdd: "",
      curEdit: "",
      curIndex: -1,
      isEditHidden: true,
      todos: [
        
      ]
    },
    created: function () {
      this.fetchAPIData()
    },
    methods: {
      fetchAPIData(){ 
        axios
          .get('http://localhost:8080/list')
          .then(response => (this.todos = response.data));
      },
      addTask(){
        //this.todos.push({text: this.curAdd})
        data = {"Data": this.curAdd}
        axios.post('http://localhost:8080/list', data)
        location.reload()
        console.log("add", this.todos)
      },
      deleteTask(index){
        //this.todos.splice(index,1)
        axios.delete('http://localhost:8080/list/'+index.toString());
        location.reload()
        console.log("delete")
      },
      editPressed(index){
        console.log("editpressed")
        this.curIndex = index
        this.isEditHidden = false
        this.curEdit = this.todos[this.curIndex].data.toString()
      },
      editTask()
      {
        console.log("edit",this.curIndex)
        data = {"Data": this.curEdit}
        axios.patch('http://localhost:8080/list/'+this.curIndex.toString(), data);
        location.reload()
        //Vue.set(this.todos,this.curIndex,{text: this.curEdit})
        this.isEditHidden = true
      }
    }
  })